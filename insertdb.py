import pandas
import mysql.connector
from mysql.connector import Error

df = pandas.read_excel(open('danhsach.xlsx','rb'), sheet_name='Sheet 1')

_STT = df['STT']
_MSSV = df['MSSV']
_Hoten = df['Hoten']
_tctl = df['SoTCTichLuy']
_Gioitinh = df['Gioitinh']
_Quequan = df['Quequan']
_DTB = df['DTB']

connection = None
try:
    connection = mysql.connector.connect(host='localhost',
                                         database='NHOM11',
                                         user='root',
                                         password='',
                                         allow_local_infile=True)

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("Your connected to database: ", record)
        for i in range(0,40):
            sql = 'INSERT INTO danhsachlop VALUES (\'' +str(_STT[i]) +'\',\''+ str(_MSSV[i]) +'\',\''+ str(_Hoten[i]) +'\',\''+ str(_tctl[i]) +'\',\''+ str(_Gioitinh[i]) +'\',\''+ str(_Quequan[i]) +'\',\''+ str(_DTB[i]) +'\')'
            print(sql)
            cursor.execute(sql)
            connection.commit()

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")
