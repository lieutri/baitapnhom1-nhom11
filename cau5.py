import mysql.connector
from mysql.connector import Error

sql = "SELECT * FROM danhsachlop WHERE Gioitinh = 'Nữ' AND DTB > 3.2;"

connection = None
try:
    connection = mysql.connector.connect(host='localhost',
                                         database='NHOM11',
                                         user='root',
                                         password='')

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("Your connected to database: ", record)
        print("Executing: ", sql)
        cursor.execute(sql)
        data = cursor.fetchall()
        if len(data) <= 0:
            print("\tKhong co du lieu!")
        for x in data:
            print(x)
        print(len(data), "record(s)")

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")