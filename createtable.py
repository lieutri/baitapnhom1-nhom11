import mysql.connector
from mysql.connector import Error

sql = '''CREATE TABLE IF NOT EXISTS danhsachlop (
  STT int(11) DEFAULT NULL,
  MSSV varchar(8) NOT NULL,
  Hoten text,
  SoTCTichLuy int(11) DEFAULT NULL,
  Gioitinh varchar(3) DEFAULT NULL,
  Quequan text,
  DTB float DEFAULT NULL,
  PRIMARY KEY (MSSV)
  ) ENGINE=MyISAM DEFAULT CHARSET=utf8;'''

connection = None
try:
    connection = mysql.connector.connect(host='localhost',
                                         database='NHOM11',
                                         user='root',
                                         password='')

    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()
        cursor.execute("select database();")
        record = cursor.fetchone()
        print("Your connected to database: ", record)
        cursor.execute(sql)

except Error as e:
    print("Error while connecting to MySQL", e)
finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")